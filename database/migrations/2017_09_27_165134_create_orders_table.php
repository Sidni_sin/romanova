<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->integer('travel_id')->nullable()->default(null);
            $table->integer('data_order_id')->nullable()->default(null);
            $table->double('price_starting', 15, 3)->default(0);
            $table->double('price_surcharge', 15, 3)->default(0);
            $table->double('price_paid', 15, 3)->default(0);
            $table->enum('status', ['true', 'surcharge', 'processed', 'false'])->default('processed');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
