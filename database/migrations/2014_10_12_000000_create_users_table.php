<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('first_name', 255)->nullable()->default(null);
            $table->string('email', 255)->unique();
            $table->enum('email_status', ['true', 'false'])->default('false');
            $table->string('password', 255);
            $table->string('phone', 255)->nullable()->default(null);
            $table->enum('status', ['true', 'false'])->nullable()->default('false');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
