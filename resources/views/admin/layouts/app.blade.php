<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (!Auth::guard('admin'))
                            <li><a href="{{ route('admin_login') }}">Login</a></li>

                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::guard('admin')->user()['name'] }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('admin_logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container"  style="background-color: #fff;">

            <div class="blog-header">
                <h1 class="blog-title">The Bootstrap Blog</h1>
                <p class="lead blog-description">The official example template of creating a blog with Bootstrap.</p>
            </div>

            <div class="row">

                <div class="col-sm-8 blog-main">
                    @yield('content')
                </div>

                @section('left_menu')
                    <div class="col-sm-3 col-sm-offset-1 blog-sidebar" style="border-left: 1px solid #bababa; ">

                        <div class="sidebar-module">
                            <h4>Админы</h4>
                            <ol class="list-unstyled">
                                <li><a href="{{ route('add_admin') }}">Добавить админа</a></li>
                                <li><a href="{{ 'admin/view' }}">Все админы</a></li>
                            </ol>
                        </div>
                        <div class="sidebar-module">
                            <h4>Заказы</h4>
                            <ol class="list-unstyled">
                                <li><a href="#">Все заказы</a></li>
                            </ol>
                        </div>
                    </div><!-- /.blog-sidebar -->
                @show

            </div>

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
