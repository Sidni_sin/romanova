@extends('admin.layouts.app')

@section('content')

    <div class="blog-post">

        <h2 class="blog-post-title">Администраторы</h2>

        <table class="table">
            <thead>
            <th>Дата</th>
            <th>Скачивания</th>
            <th>Прибыль с загрузок</th>
            <th>Прибыль рефералов</th>
            <th>Общий зарабаток</th>
            </thead>
            <tbody>

            <tr class="odd">
                <td>1</td>
                <td>2</td>
                <td>21</td>
                <td><?= "0" ?></td>
                <td>2</td>

            </tr>

            </tbody>
        </table>

    </div>

@endsection