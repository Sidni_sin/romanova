<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth.admin']], function(){

    Route::get('admin/home',  function (){
        return Auth::guard('admin')->user();
    });

    Route::get('admin','Admin\AdminController@index');

    Route::get('admin/add','Admin\AdminController@add_admin_form');
    Route::post('admin/add','Admin\AdminController@add_admin')->name('add_admin');
    Route::get('admin/view_admin','Admin\AdminController@view_admin');

    Route::get('admin/order','Admin\AdminController@order');
    //Route::post('admin/add','Admin\AdminController@add_admin');

});

Route::get('admin/login',['as' => 'admin_login','uses' => 'Admin\Auth\LoginController@showLoginForm']);
Route::post('/admin/login',['uses' => 'Admin\Auth\LoginController@login']);
Route::get('/admin/logout',['as' => 'admin_logout','uses' => 'AuthAdmin\LoginController@logout']);