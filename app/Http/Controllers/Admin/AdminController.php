<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }

    public function add_admin_form()
    {
        return view('admin.add_admin');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function add_admin(Request $request)
    {

        $this->validate($request, [
           'name' => 'required|max:255',
           'email' => 'required|unique:admins,email',
           'password' => 'required|max:255',
        ]);

        $data = $request->all();
        $data['password'] = Hash::make($data['password']);

        $admin = new Admin();
        $admin->fill($data);

        if($admin->save()){
            $response = redirect('admin');
        }else{
            $response = 'Error';
        }

        return $response;

    }

    public function view_admin()
    {

    }

}
